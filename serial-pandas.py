import requests
import csv
from dagster import job, op, get_dagster_logger
import pandas as pd


@op
def download_cereals() -> pd.DataFrame:
  response = requests.get("https://docs.dagster.io/assets/cereal.csv")
  lines = response.text.split("\n")
  cereals = [row for row in csv.DictReader(lines)]
  get_dagster_logger().info(f"From CSV: Found {len(cereals)} cereals")
  df = pd.DataFrame(cereals)
  get_dagster_logger().info(f"From DataFrame: Found {len(df)} cereals")
  return df


@op
def find_sugariest(cereals: pd.DataFrame) -> str:
  sorted_by_sugar = cereals.sort_values(by=['sugars'])
  # Note: Result is incorrect as result will be 9 grams due to string sorting, instead of numeric sorting
  get_dagster_logger().info(f'{sorted_by_sugar.iloc[-1]["name"]} is the sugariest cereal with {sorted_by_sugar.iloc[-1]["sugars"]} grams')
  return sorted_by_sugar.iloc[-1]["name"]


@job
def serial():
  find_sugariest(download_cereals())
