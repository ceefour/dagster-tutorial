To visualize your job (which only has one op) in Dagit, just run the following. Make sure you're in the directory in which you've saved the job file:

dagit -f hello_cereal.py
You'll see output like

Serving dagit on http://127.0.0.1:3000 in process 70635
You should be able to navigate to http://127.0.0.1:3000 in your web browser and view your job. It isn't very interesting yet, because it only has one op.
