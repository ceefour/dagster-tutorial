import requests
import csv
from dagster import job, op, get_dagster_logger


@op
def download_cereals() -> DataFrame:
  response = requests.get("https://docs.dagster.io/assets/cereal.csv")
  lines = response.text.split("\n")
  cereals = [row for row in csv.DictReader(lines)]
  get_dagster_logger().info(f"Found {len(cereals)} cereals")
  return cereals


@op
def find_sugariest(cereals):
  sorted_by_sugar = sorted(cereals, key=lambda cereal: cereal['sugars'])
  get_dagster_logger().info(f'{sorted_by_sugar[-1]["name"]} is the sugariest cereal')


@job
def serial():
  find_sugariest(download_cereals())
